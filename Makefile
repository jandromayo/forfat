SRCPATH := src
BINPATH := bin
DOCPATH := doc

SOURCES := $(shell find $(SRCPATH) -name "*.java")
CLASSES := $(patsubst $(SRCPATH)/%.java, $(BINPATH)/%.class, $(SOURCES))

.PHONY: all doc compile clean

all: Forfat.jar doc

Forfat.jar: $(CLASSES)
	jar cvef uva.forense.forfat.Forfat $@ -C $(BINPATH) .

compile: $(CLASSES)

doc:
	mkdir -p $(DOCPATH)
	javadoc -version -author -d $(DOCPATH) -classpath $(BINPATH) $(SOURCES)
	
bin:
	mkdir -p $(BINPATH)
	
clean:
	-$(RM) -r $(BINPATH)
	-$(RM) -r $(DOCPATH)
	-$(RM) Forfat.jar
	
$(BINPATH)/%.class: $(SRCPATH)/%.java bin Makefile
	javac -sourcepath $(SRCPATH) -d $(BINPATH) $<