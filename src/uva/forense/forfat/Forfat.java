package uva.forense.forfat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.UserPrincipal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import uva.forense.forfat.diff.dirdiff.DirDiff;
import uva.forense.forfat.diff.dirdiff.DirDiffAnalizer;
import uva.forense.forfat.diff.filediff.DiffChunk;
import uva.forense.forfat.diff.filediff.FileDiffAnalizer;
import uva.forense.forfat.findext.FindExt;
import uva.forense.forfat.findtime.FindTime;

/**
 * Clase principal de Forfat que valida la entrada y en funcion de esta crea una instancia 
 * de una clase u otra.
 * 
 * @author Alonso Mayo, Alejandro
 * @author Iglesias Garcia, Jesus
 * @version 1.0
 */
public class Forfat {


	/**
	 * Metodo principal de Forfat.
	 * 
	 * @param args Parametros de entrada
	 */
	public static void main(String[] args) {

		// Se valida la entrada de usuario
		if (args.length < 1) {
			System.err.println("Argumentos insuficientes.");
			System.err.println("Eliga la opcion --help para mostrar la ayuda.");
			System.exit(1);
		}

		if (args[0].equals("--help")) {
			
			if (args.length != 1) {
				System.err.println("Argumentos incorrectos.");
				System.err
						.println("Debe indicar solamente la opcion --help para mostrar la ayuda.");
				System.exit(1);
			}
			printHelp();
			
		} else if (args[0].equals("--diff-files")) {

			if (args.length != 3) {
				System.err.println("Argumentos insuficientes o incorrectos.");
				System.err
						.println("Eliga la opcion --help para mostrar la ayuda.");
				System.exit(1);
			}

			fileDiff(args[1], args[2]);

		} else if (args[0].equals("--diff-dirs")) {

			if (args.length != 3) {
				System.err.println("Argumentos insuficientes o incorrectos.");
				System.err
						.println("Eliga la opcion --help para mostrar la ayuda.");
				System.exit(1);
			}

			dirDiff(args[1], args[2]);

		} else if (args[0].equals("--find-ext")) {
			if (args.length != 3) {
				System.err.println("Argumentos insuficientes o incorrectos.");
				System.err
						.println("Eliga la opcion --help para mostrar la ayuda.");
				System.exit(1);
			}
			findExt(args[1], args[2]);
			
		} else if (args[0].equals("--find-time")) {
			if (args.length != 5) {
				System.err.println("Argumentos insuficientes o incorrectos.");
				System.err
						.println("Eliga la opcion --help para mostrar la ayuda.");
				System.exit(1);
			}

			if (args[1].equals("-c")) {
				findCreationTime(args[2], args[3], args[4]);
			} else if (args[1].equals("-a")) {
				findAcccessTime(args[2], args[3], args[4]);
			} else if (args[1].equals("-m")) {
				findModificationTime(args[2], args[3], args[4]);
			} else {
				System.err.println("Opcion desconocida.");
				System.err
						.println("Eliga la opcion --help para mostrar la ayuda.");
				System.exit(1);
			}
		} else {
			System.err.println("Funcion desconocida.");
			System.err.println("Eliga la opcion --help para mostrar la ayuda.");
			System.exit(1);
		}
	}

	/**
	 * Metodo que muestra la ayuda de Forfat.
	 */
	private static void printHelp() {
		
		System.out.println("Forensic File Analizer Tool (FORFAT). Uso:");
		System.out.println("forfat --help - Muestra la ayuda actual.");
		System.out
				.println("forfat --diff-files <fich origen> <fich revision> - Muestra las diferencias entre ficheros.");
		System.out
				.println("forfat --diff-dirs <dir1> <dir2> - Muestra las diferencias entre directorios.");
		System.out
				.println("forfat --find-time -c <fecha inicio> <fecha fin> <dir> - Muestra los archivos creados entre dos fechas dadas.");
		System.out
				.println("forfat --find-time -a <fecha inicio> <fecha fin> <dir> - Muestra los archivos accedidos entre dos fechas dadas.");
		System.out
				.println("forfat --find-time -m <fecha inicio> <fecha fin> <dir> - Muestra los archivos modificados entre dos fechas dadas.");
		System.out
				.println("forfat --find-ext <dir> <extension> - Muestra los archivos dentro del directorio con la extension dada.");
		System.out.println();
		System.out.println("Las fechas se introducen con el siguiente formato: dd-MM-yyyy.");
	};

	/**
	 * Tratamiento de busqueda de diferencias entre ficheros.
	 * 
	 * @param origPath Ruta del fichero origen
	 * @param revPath Ruta del fichero a revisar
	 */
	private static void fileDiff(String origPath, String revPath) {
		
		File orig = new File(origPath);
		File rev = new File(revPath);

		// Se comprueba si los ficheros indicados existen
		if (!orig.exists()) {
			System.err.println("No existe el archivo " + origPath+ ".");
			System.exit(1);
		} else if (!rev.exists()) {
			System.err.println("No existe el archivo " + revPath+ ".");
			System.exit(1);
		}

		// Se comprueba si las rutas indicadas pertenecen a ficheros
		if (!orig.isFile()) {
			System.err.println(origPath + " no es un fichero.");
			System.exit(1);
		} else if (!rev.isFile()) {
			System.err.println(revPath + " no es un fichero.");
			System.exit(1);
		}

		// Se obtienen las diferencias entre ficheros
		FileDiffAnalizer diffAnalizer = new FileDiffAnalizer(orig, rev);
		for (DiffChunk diff : diffAnalizer.getDifferences()) {
			System.out.println(diff);
		}
	}

	/**
	 * Tratamiento de busqueda de diferencias entre directorios.
	 * 
	 * @param origPath Ruta del directorio origen
	 * @param revPath Ruta del directorio a revisar
	 */
	private static void dirDiff(String origPath, String revPath) {
		
		File orig = new File(origPath);
		File rev = new File(revPath);

		// Se comprueba si los directorios indicados existen
		if (!orig.exists()) {
			System.err.println("No existe el directorio " + origPath + ".");
			System.exit(1);
		} else if (!rev.exists()) {
			System.err.println("No existe el directorio " + revPath + ".");
			System.exit(1);
		}

		// Se comprueba si las rutas indicadas pertenecen a directorios
		if (!orig.isDirectory()) {
			System.err.println(orig.getAbsolutePath() + " no es un directorio.");
			System.exit(1);
		} else if (!rev.isDirectory()) {
			System.err.println(rev.getAbsolutePath() + " no es un directorio.");
			System.exit(1);
		}

		// Se obtienen las diferencias entre directorios
		DirDiffAnalizer diffAnalizer = new DirDiffAnalizer(orig, rev);
		for (DirDiff diff : diffAnalizer.getDifferences()) {
			System.out.println(diff);
		}

	}

	/**
	 * Tratamiento de la busqueda de ficheros de una determinada extension.
	 * 
	 * @param dirPath Ruta del directorio donde se realiza la busqueda
	 * @param ext Extension a buscar
	 */
	private static void findExt(String dirPath, String ext) {
		
		File dir = new File(dirPath);
		
		// Se comprueba que la ruta indique un directorio y exista
		if (dir.exists() && dir.isDirectory()) {
			
			FindExt fext = new FindExt(dir, ext);
			List<File> files = fext.getFiles();
			List<Integer> copyIndex = new ArrayList<>();
			
			// Se muestra la lista de los ficheros encontrados
			for (int i = 0; i < files.size(); i++) {
				System.out.println("[" + (i + 1) + "] " + files.get(i));
			}
			
			// Se comprueba si se encontraron ficheros 
			if (files.size() != 0) {
				
				// Se indican los ficheros a copiar
				System.out.print("Indique los numeros de los archivos a copiar separados por espacios (en blanco para ninguno): ");
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));			
				try {
					Scanner scan = new Scanner(br.readLine());
					if (scan.hasNext() && !scan.hasNextInt()) {
						System.err.println("Los datos introducidos deben ser numericos.");
					} else {
						while (scan.hasNextInt()) {
							copyIndex.add(scan.nextInt() - 1);
						}
					}
					scan.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				// Se indica el directorio donde se realiza la copia y se efectua la accion de copiar
				if (!copyIndex.isEmpty()) {
					System.out.print("Indique el directorio de salida (en blanco para el actual): ");
					try {
						String path = br.readLine();					
						// Se comprueba si se no se introdujo una ruta destino
						if ( (path == null) || (path.isEmpty()) ) {
							// Se obtiene la ruta actual
							path = System.getProperty("user.dir");
						}
						
						fext.copyFiles(copyIndex, path);
						System.out.println("Copia de archivos realizada correctamente.");
					} catch (IOException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}else {
				System.out.println("No se han encontrado ficheros en la ruta " + dirPath + " con la extension " + ext +".");
				System.exit(1);
			}
		} else {
			System.err.println("Directorio invalido.");
			System.exit(1);
		}
	}

	/**
	 * Tratamiento de la busqueda de ficheros creados en un rango de fechas determinado.
	 * 
	 * @param init Fecha inicial a buscar
	 * @param end Fecha final a buscar
	 * @param dir Ruta del directorio donde se realiza la busqueda
	 * 
	 */
	private static void findCreationTime(String init, String end, String dir) {
		
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		
		try {
			// Se parsean las fechas introducidas
			Date initDate = format.parse(init);
			Date endsDate = format.parse(end);
			
			if (initDate.equals(endsDate)){
				System.err.println("Las fechas de inicio y fin no pueden coincidir.");
				System.exit(1);
			}
			
			File fdir = new File(dir);
				
			// Se comprueba que la ruta indique un directorio y exista
			if (fdir.exists() && fdir.isDirectory()) {
			
				FindTime ft = new FindTime(initDate, endsDate, fdir);
				List<File> files = ft.findByCreation();
				List<Integer> copyIndex = new ArrayList<>();
				
				// Se muestra la lista de los ficheros encontrados y su propietario
				for (int i = 0; i < files.size(); i++) {
					String owner = getOwner(files.get(i));
					System.out.println("[" + (i + 1) + "] " + files.get(i) + " -- Propietario: " + owner);
				}
				
				// Se comprueba si se encontraron ficheros 
				if (files.size() != 0) {
					
					// Se indican los ficheros a copiar
					System.out.print("Indique los numeros de los archivos a copiar separados por espacios (en blanco para ninguno): ");
					BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
					try {
						Scanner scan = new Scanner(br.readLine());
						if (scan.hasNext() && !scan.hasNextInt()) {
							System.err.println("Los datos introducidos deben ser numericos.");
						} else {
							while (scan.hasNextInt()) {
								copyIndex.add(scan.nextInt() - 1);
							}
						}
						scan.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					// Se indica el directorio donde se realiza la copia y se efectua la accion de copiar
					if (!copyIndex.isEmpty()) {
						System.out.print("Indique el directorio de salida (en blanco para el actual): ");
						try {
							String path = br.readLine();				
							// Se comprueba si se no se introdujo una ruta destino
							if ( (path == null) || (path.isEmpty()) ) {
								// Se obtiene la ruta actual
								path = System.getProperty("user.dir");
							}
							
							for(Integer i:copyIndex){
								copy(files.get(i), path, fdir);
							}
						} catch (IOException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						}
						System.out.println("Copia de archivos realizada correctamente.");
					}
				}else {
					System.out.println("No se han encontrado ficheros creados en la ruta " + dir + " entre el rango de fechas " + init + " y " + end + ".");
					System.exit(1);
				}
			} else {
				System.err.println("Directorio invalido.");
				System.exit(1);
			}
		} catch (ParseException e) {
			System.err.println("Fechas introducidas en formato incorrecto.");
			System.err.println("Eliga la opcion --help para mostrar la ayuda.");
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Tratamiento de la busqueda de ficheros accedidos en un rango de fechas determinado.
	 * 
	 * @param init Fecha inicial a buscar
	 * @param end Fecha final a buscar
	 * @param dir Ruta del directorio donde se realiza la busqueda
	 */
	private static void findAcccessTime(String init, String end, String dir) {
		
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		
		try {
			// Se parsean las fechas introducidas
			Date initDate = format.parse(init);
			Date endsDate = format.parse(end);
			
			if (initDate.equals(endsDate)){
				System.err.println("Las fechas de inicio y fin no pueden coincidir.");
				System.exit(1);
			}
			
			File fdir = new File(dir);
							
			// Se comprueba que la ruta indique un directorio y exista
			if (fdir.exists() && fdir.isDirectory()) {
			
				FindTime ft = new FindTime(initDate, endsDate, fdir);
				List<File> files = ft.findByAccess();
				List<Integer> copyIndex = new ArrayList<>();
				
				// Se muestra la lista de los ficheros encontrados y su propietario
				for (int i = 0; i < files.size(); i++) {
					String owner = getOwner(files.get(i));
					System.out.println("[" + (i + 1) + "] " + files.get(i) + " -- Propietario: " + owner);
				}
				
				// Se comprueba si se encontraron ficheros 
				if (files.size() != 0) {
					
					// Se indican los ficheros a copiar
					System.out.print("Indique los numeros los archivos a copiar separados por espacios (en blanco para ninguno): ");
					BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
					try {
						Scanner scan = new Scanner(br.readLine());
						if (scan.hasNext() && !scan.hasNextInt()) {
							System.err.println("Los datos introducidos deben ser numericos.");
						} else {
							while (scan.hasNextInt()) {
								copyIndex.add(scan.nextInt() - 1);
							}
						}
						scan.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					// Se indica el directorio donde se realiza la copia y se efectua la accion de copiar
					if (!copyIndex.isEmpty()) {
						System.out.print("Indique el directorio de salida (en blanco para el actual): ");
						try {
							String path = br.readLine();
							// Se comprueba si se no se introdujo una ruta destino
							if ( (path == null) || (path.isEmpty()) ) {
								// Se obtiene la ruta actual
								path = System.getProperty("user.dir");
							}
							
							for(Integer i:copyIndex){
								copy(files.get(i), path, fdir);
							}
						} catch (IOException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						}
						System.out.println("Copia de archivos realizada correctamente.");
					}
				}else {
					System.out.println("No se han encontrado ficheros accedidos en la ruta " + dir + " entre el rango de fechas " + init + " y " + end + ".");
					System.exit(1);
				}
			} else {
				System.err.println("Directorio invalido.");
				System.exit(1);
			}
		} catch (ParseException e) {
			System.err.println("Fechas introducidas en formato incorrecto.");
			System.err.println("Eliga la opcion --help para mostrar la ayuda.");
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Tratamiento de la busqueda de ficheros modificados en un rango de fechas determinado.
	 * 
	 * @param init Fecha inicial a buscar
	 * @param end Fecha final a buscar
	 * @param dir Ruta del directorio donde se realiza la busqueda
	 */
	private static void findModificationTime(String init, String end, String dir) {
		
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		
		try {
			// Se parsean las fechas introducidas
			Date initDate = format.parse(init);
			Date endsDate = format.parse(end);
			
			if (initDate.equals(endsDate)){
				System.err.println("Las fechas de inicio y fin no pueden coincidir.");
				System.exit(1);
			}
			
			File fdir = new File(dir);
								
			// Se comprueba que la ruta indique un directorio y exista
			if (fdir.exists() && fdir.isDirectory()) {

				FindTime ft = new FindTime(initDate, endsDate, fdir);
				List<File> files = ft.findByModification();
				List<Integer> copyIndex = new ArrayList<>();
				
				// Se muestra la lista de los ficheros encontrados y su propietario
				for (int i = 0; i < files.size(); i++) {
					String owner = getOwner(files.get(i));
					System.out.println("[" + (i + 1) + "] " + files.get(i) + " -- Propietario: " + owner);
				}
				
				// Se comprueba si se encontraron ficheros 
				if (files.size() != 0) {

					// Se indican los ficheros a copiar
					System.out.print("Indique los numeros de los archivos a copiar separados por espacios (en blanco para ninguno): ");
					BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
					try {
						Scanner scan = new Scanner(br.readLine());
						if (scan.hasNext() && !scan.hasNextInt()) {
							System.err.println("Los datos introducidos deben ser numericos.");
						} else {
							while (scan.hasNextInt()) {
								copyIndex.add(scan.nextInt() - 1);
							}
						}
						scan.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					// Se indica el directorio donde se realiza la copia y se efectua la accion de copiar
					if (!copyIndex.isEmpty()) {
						System.out.print("Indique el directorio de salida (en blanco para el actual): ");
						try {
							String path = br.readLine();
							// Se comprueba si se no se introdujo una ruta destino
							if ( (path == null) || (path.isEmpty()) ) {
								// Se obtiene la ruta actual
								path = System.getProperty("user.dir");
							}
							
							for(Integer i:copyIndex){
								copy(files.get(i), path, fdir);
							}
						} catch (IOException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						}
						System.out.println("Copia de archivos realizada correctamente.");
					}
				}else {
					System.out.println("No se han encontrado ficheros modificados en la ruta " + dir + " entre el rango de fechas " + init + " y " + end + ".");
					System.exit(1);
				}
			} else {
				System.err.println("Directorio invalido.");
				System.exit(1);
			}
		} catch (ParseException e) {
			System.err.println("Fechas introducidas en formato incorrecto.");
			System.err.println("Eliga la opcion --help para mostrar la ayuda.");
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Implementacion de la copia de ficheros para la funcionalidad de busqueda en un rango de fechas determinado.
	 * 
	 * @param file Fichero a copiar
	 * @param path Ruta donde se copiara el fichero
	 * @param origDir Directorio donde se realiza la busqueda
	 * 
	 * @throws Exception
	 */
	private static void copy(File file, String path, File origDir)
			throws Exception {
		
		Scanner entrada = new Scanner (System.in); 
		File dir = new File(path);
		String nombre;
		int c, extension;
		
		// Se comprueba que la ruta destino exista y no sea un directorio
		if (dir.exists() && !dir.isDirectory()) {
			System.err.println("La ubicacion destino no es un directorio. Copia fallida.");
			System.exit(1);
		}

		File dir2 = new File(dir, file
				.getAbsolutePath()
				.replace("\\", "/")
				.replaceFirst(
						origDir.getAbsolutePath().replace("\\", "/") + "/", "")
				.replaceAll(file.getName(), ""));
		
		// Si el directorio no existe, se crea
		if (!dir2.exists()) {
			dir2.mkdirs();
		}

		File copy = new File(dir2, file.getName());
		
		// Se comprueba si existe un fichero con el mismo nombre
		if (copy.exists()){
			
			do {
				System.out.print("Existe un fichero con el mismo nombre en la ruta indicada. Si desea mantener el fichero, indique su nombre y extension "
					+ "(Formato: <nombre.extension>) (en blanco para sobrescribir):");
				nombre = entrada.nextLine();
				
				// Se comprueba si el string tiene extension
				extension = nombre.lastIndexOf(".");

			} while( (extension == -1) && (nombre != null) && !(nombre.isEmpty()));
		
			// Se comprueba si no se introdujo el nuevo nombre
			if ( (nombre == null) || (nombre.isEmpty()) ) {
			
				FileInputStream in = new FileInputStream(file);
				FileOutputStream out = new FileOutputStream(copy);
				
				// Se copia del contenido del fichero origen al fichero destino
				while ((c = in.read()) != -1)
					out.write(c);

				// Se cierran ambos ficheros
				in.close();
				out.close();
			
			} else{ // Nuevo nombre introducido
				
				File copy2 = new File(dir2, nombre);
				
				FileInputStream in = new FileInputStream(file);
				FileOutputStream out = new FileOutputStream(copy2);
				
				// Se copia del contenido del fichero origen al fichero destino
				while ((c = in.read()) != -1)
					out.write(c);
				
				// Se cierran ambos ficheros
				in.close();
				out.close();
			}
		} else { // No existe un fichero con el mismo nombre	
				
			FileInputStream in = new FileInputStream(file);
			FileOutputStream out = new FileOutputStream(copy);
			
			// Se copia del contenido del fichero origen al fichero destino
			while ((c = in.read()) != -1)
				out.write(c);
			
			// Se cierran ambos ficheros
			in.close();
			out.close();
		}
		
		// Se cierra el scanner
		entrada.close();
	}
	
	/**
	 * Obtencion del propietario del fichero.
	 * 
	 * @param file Fichero a examinar
	 * 
	 * @throws IOException
	 */
	private static String getOwner(File fichero) throws IOException {

		// Se obtiene el poseedor del fichero
		Path path = Paths.get(fichero.getPath());
		UserPrincipal owner = Files.getOwner(path);
		
		return owner.getName();
	}
}
