package uva.forense.forfat.findtime;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Clase que encuentra los ficheros en un rango de fechas definidas por el usuario.
 * 
 * @author Alonso Mayo, Alejandro
 * @author Iglesias Garcia, Jesus
 * @version 1.0
 */
public class FindTime {
	
	// Declaracion de variables
	private File dir;
	private FileTime initTime;
	private FileTime endTime;
	
	/**
	 * Constructor de la clase FindTime.
	 * 
	 * @param initTime Fecha inicial de busqueda
	 * @param endTime Fecha final de busqueda
	 * @param dir Directorio donde se realiza la busqueda
	 */
	public FindTime(Date initTime, Date endTime, File dir){
		this.initTime = FileTime.fromMillis(initTime.getTime());
		this.endTime = FileTime.fromMillis(endTime.getTime());
		this.dir = dir;
	}
	
	/**
	 * Devuelve la lista de ficheros creados en el rango especificado.
	 * 
	 * @return Lista con los ficheros creados en el rango especificado
	 */
	public List<File> findByCreation(){
		return findByCreation(dir);
	}
	
	/**
	 * Devuelve la lista de ficheros accedidos en el rango especificado.
	 * 
	 * @return Lista con los ficheros accedidos en el rango especificado
	 */
	public List<File> findByAccess(){
		return findByAccess(dir);
	}
	
	/**
	 * Devuelve la lista de ficheros modificados en el rango especificado.
	 * 
	 * @return Lista con los ficheros modificados en el rango especificado
	 */
	public List<File> findByModification(){
		return findByModification(dir);
	}
	
	
	/**
	 * Obtiene los ficheros que han sido creados en el rango especificado.
	 * 
	 * @param file Fichero donde se realiza la busqueda
	 * 
	 * @return files Lista con los ficheros creados en el rango especificado
	 */
	private List<File> findByCreation(File file){
		
		List<File> files = new ArrayList<>();
		
		// Se comprueba que sea un directorio
		if (file.isDirectory()){
			// Se indica que cada fichero individual debe ser comprobado
			for (File f1:file.listFiles()){
				files.addAll(findByCreation(f1));
			}
		} else { 
			Path path = Paths.get(file.getPath());
			BasicFileAttributes attrs;
			try {
				// Se leen los atributos del fichero
				attrs = Files.readAttributes(path, BasicFileAttributes.class);
				FileTime time = attrs.creationTime();
				// Se comparan las fechas del fichero con las indicadas
				if (time.compareTo(initTime) >= 0 && time.compareTo(endTime) <= 0){
					files.add(file);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return files;
	}
	
	/**
	 * Obtiene los ficheros que han sido modificados en el rango especificado.
	 * 
	 * @param file Fichero donde se realiza la busqueda
	 * 
	 * @return files Lista con los ficheros modificados en el rango especificado
	 */
	private List<File> findByModification(File file){
		
		List<File> files = new ArrayList<>();
		
		// Se comprueba que sea un directorio
		if (file.isDirectory()){
			// Se indica que cada fichero individual debe ser comprobado
			for (File f1:file.listFiles()){
				files.addAll(findByModification(f1));
			}
		} else {
			Path path = Paths.get(file.getPath());
			BasicFileAttributes attrs;
			try {
				// Se leen los atributos del fichero
				attrs = Files.readAttributes(path, BasicFileAttributes.class);
				FileTime time = attrs.lastModifiedTime();
				// Se comparan las fechas del fichero con las indicadas
				if (time.compareTo(initTime) >= 0 && time.compareTo(endTime) <= 0){
					files.add(file);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return files;
	}
	
	/**
	 * Obtiene los ficheros que han sido accedidos en el rango especificado.
	 * 
	 * @param file Fichero donde se realiza la busqueda
	 * 
	 * @return files Lista con los ficheros accedidos en el rango especificado
	 */
	private List<File> findByAccess(File file){
		
		List<File> files = new ArrayList<>();
		
		// Se comprueba que sea un directorio				
		if (file.isDirectory()){
			// Se indica que cada fichero individual debe ser comprobado
			for (File f1:file.listFiles()){
				files.addAll(findByAccess(f1));
			}
		} else {
			Path path = Paths.get(file.getPath());
			BasicFileAttributes attrs;
			try {
				// Se leen los atributos del fichero
				attrs = Files.readAttributes(path, BasicFileAttributes.class);
				FileTime time = attrs.lastAccessTime();
				// Se comparan las fechas del fichero con las indicadas
				if (time.compareTo(initTime) >= 0 && time.compareTo(endTime) <= 0){
					files.add(file);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return files;
	}
}
