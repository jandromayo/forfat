
package uva.forense.forfat.diff.dirdiff;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementa la busqueda de diferencias entre dos directorios.
 * 
 * @author Alonso Mayo, Alejandro
 * @author Iglesias Garcia, Jesus
 * 
 * @version 1.0
 */
public class DirDiffAnalizer {
	
	// Declaracion de variables
	private File origDir;
	private File revDir;
	
	/**
	* Constructor a partir de directorios de la clase DirDiffAnalizer.
	* 
	* @param origDir Directorio de entrada origen
	* @param revDir Directorio de entrada a revisar
	*/
	public DirDiffAnalizer(File origDir, File revDir) {
		this.origDir = origDir;
		this.revDir = revDir;
	}

	/**
	 * Devuelve las diferencias entre dos directorios.
	 * 
	 * @return diffs Lista con las diferencias encontradas
	 */
	public List<DirDiff> getDifferences(){
		
		List<DirDiff> diffs = new ArrayList<DirDiff>();
		File[] files1 = origDir.listFiles();
		File[] files2 = revDir.listFiles();
		
		for (File f1:files1){
			boolean exist = false;
			for(File f2:files2){
				if (f1.getName().equals(f2.getName())){
					exist = true;
					if (f1.isDirectory()){
						DirDiffAnalizer rdir = new DirDiffAnalizer(f1, f2);
						List<DirDiff> rdiffs = rdir.getDifferences();
						diffs.add(new ChangeDirDiff(f1.getName(), rdiffs));
					}
				}
			}
			if (!exist){
				String diff = f1.getName();
				if (f1.isDirectory()) diff += "/";
				diffs.add(new RemoveDirDiff(diff));
			}
		}
		for (File f1:files2){
			boolean exist = false;
			for(File f2:files1){
				if (f1.getName().equals(f2.getName())){
					exist = true;
				}
			}
			if (!exist){
				String diff = f1.getName();
				if (f1.isDirectory()) diff += "/";
				diffs.add(new AddedDirDiff(diff));
			}
		}
		return diffs;
	}
}		