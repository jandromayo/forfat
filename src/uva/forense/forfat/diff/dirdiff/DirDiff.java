package uva.forense.forfat.diff.dirdiff;

/**
 * Representa una diferencia en el arbol de directorios a comparar.
 * 
 * @author Alonso Mayo, Alejandro
 * @author Iglesias Garcia, Jesus
 * @version 1.0
 */
public abstract class DirDiff {
	
	// Declaracion de variables
	private String diff;
	
	/**
	 * Constructor a partir del dato de la diferencia.
	 * 
	 * @param diff Nombre de la diferencia. Nombre del archivo introducido.
	 */
	public DirDiff(String diff){
		this.diff = diff;
	}
	
	/**
	 * Devuelve la diferencia.
	 * 
	 * @return diff Diferencia
	 */
	public String getDiff(){
		return diff;
	}
	
	/**
	 * Metodo que permite mostrar la informacion completa de un objeto.
	 */
	public abstract String toString();
}

