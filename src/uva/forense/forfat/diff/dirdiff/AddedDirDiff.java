package uva.forense.forfat.diff.dirdiff;


/**
 * Representa los archivos introducidos en el arbol de directorios a comparar.
 * 
 * @author Alonso Mayo, Alejandro
 * @author Iglesias Garcia, Jesus
 * @version 1.0
 */
public class AddedDirDiff extends DirDiff {

	/**
	 * Constructor a partir del dato de la diferencia.
	 * 
	 * @param diff Nombre de la diferencia
	 */
	public AddedDirDiff(String diff) {
		super(diff);
	}

	/**
	 * Asocia a todos los objetos el texto representativo.
	 * 
	 * @return result Texto formateado a imprimir por pantalla
	 */
	@Override
	public String toString() {
		
		// Se formatea la salida
		String result = "";
		result += "> " + getDiff();
		return result;
	}	
}