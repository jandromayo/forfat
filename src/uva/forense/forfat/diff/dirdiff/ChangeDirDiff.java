package uva.forense.forfat.diff.dirdiff;

import java.util.List;

/**
 * Representa los directorios modificados en el arbol de directorios a comparar.
 * 
 * @author Alonso Mayo, Alejandro
 * @author Iglesias Garcia, Jesus
 * @version 1.0
 */
public class ChangeDirDiff extends DirDiff {

	// Declaracion de variables
	List<DirDiff> rdiffs;
	
	/**
	 * Constructor de la clase AddedDirDiff.
	 * 
	 * @param diff Nombre del directorio modificado.
	 * @param rdiffs Diferencias dentro del directorio.
	 */
	public ChangeDirDiff(String diff, List<DirDiff> rdiffs) {
		super(diff);
		this.rdiffs = rdiffs;
	}

	/**
	 * Asocia a todos los objetos el texto representativo.
	 * 
	 * @return result Texto formateado a imprimir por pantalla
	 */
	@Override
	public String toString() {
		
		// Se formatea la salida
		String result = "";
		result += "= " + getDiff() + "/";
		for(DirDiff diff:rdiffs){
			result += "\n    " + diff;
		}
		return result;
	}
}
