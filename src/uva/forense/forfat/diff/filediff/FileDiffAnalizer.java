package uva.forense.forfat.diff.filediff;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Implementa la busqueda de diferencias entre dos ficheros.
 * 
 * @author Alonso Mayo, Alejandro
 * @author Iglesias Garcia, Jesus
 * @version 1.0
 */
public class FileDiffAnalizer {
	
	// Declaracion de variables
	private List<Line> origLines = new ArrayList<Line>();
	private List<Line> revLines = new ArrayList<Line>();
	
	/**
	 * Constructor a partir de ficheros.
	 * 
	 * @param origFile Lista de lineas del archivo origen
	 * @param revFile Lista de lineas del archivo revision
	 */
	public FileDiffAnalizer(File origFile, File revFile) {
		this(getStringList(origFile), getStringList(revFile));
	}

	/**
	 * Constructor a partir de lista de lineas.
	 * 
	 * @param origLines Lista de lineas del archivo origen
	 * @param revLines Lista de lineas del archivo revision
	 */
	public FileDiffAnalizer(List<String> origLines, List<String> revLines) {
		
		for (int i = 0; i < origLines.size(); i++) {
			this.origLines.add(new Line(i, origLines.get(i)));
		}
		for (int i = 0; i < revLines.size(); i++) {
			this.revLines.add(new Line(i, revLines.get(i)));
		}
	}

	/**
	 * Devuelve las diferencias entre dos ficheros.
	 * 
	 * @return Lista con diferencias encontradas
	 */
	public List<DiffChunk> getDifferences() {
		
		List<DiffChunk> result = new ArrayList<DiffChunk>();
		List<Line> origLines = new ArrayList<Line>();
		List<Line> revLines = new ArrayList<Line>();
		origLines.addAll(this.origLines);
		revLines.addAll(this.revLines);
		
		DiffChunk chunk;
		deleteEquals(origLines, revLines);
		
		// Se obtiene el grupo de lineas borradas
		chunk = findDeletes(origLines, revLines);
		if (chunk != null) {
			result.add(chunk);
		}
		// Se obtiene el grupo de lineas insertadas
		chunk = findAppends(origLines, revLines);
		if (chunk != null) {
			result.add(chunk);
		}
		
		// Mientras queden lineas por revisar, se buscan diferencias
		while (origLines.size() > 0 && revLines.size() > 0) {
			
			deleteEquals(origLines, revLines);
			chunk = findDeletes(origLines, revLines);
			
			if (chunk != null) {
				result.add(chunk);
			}
			chunk = findAppends(origLines, revLines);
			if (chunk != null) {
				result.add(chunk);
			}
		}
		return result;
	}

	/**
	 * Encuentra diferencias en insercion.
	 * 
	 * @param origLine Lista de lineas del fichero original
	 * @param revLines Lista de lineas del fichero a revisar
	 * 
	 * @return chunk Grupo de lineas insertadas
	 */
	private DiffChunk findAppends(List<Line> origLines, List<Line> revLines) {
		
		DiffChunk chunk = null;
		boolean equals = false;
		List<Line> diffs = new ArrayList<Line>();
		int changedLine = this.origLines.size();
		
		// Se buscan lineas donde se haya insertado un fragmento
		if (origLines.size() > 0) {
			
			Line orig = origLines.get(0);
			changedLine = orig.getNumber();
			
			while (revLines.size() > 0 && !equals) {
				Line rev = revLines.get(0);
				
				if (orig.getText().equals(rev.getText())) {
					equals = true;
				} else { // Si la linea es distinta, existe diferencia
					diffs.add(rev);
					revLines.remove(0);
				}
			}
		} else if (revLines.size() > 0) {
			diffs.addAll(revLines);
		}
		// Si existen diferencias, se crea una instancia de AppendDiffChunk
		if (diffs.size() > 0)
			chunk = new AppendDiffChunk(diffs, changedLine);
		return chunk;
	}

	/**
	 * Encuentra diferencias en eliminaciones.
	 * 
	 * @param origLine Lista de lineas del fichero original
	 * @param revLines Lista de lineas del fichero a revisar
	 * 
	 * @return chunk Grupo de lineas borradas
	 */
	private DiffChunk findDeletes(List<Line> origLines, List<Line> revLines) {
		
		DiffChunk chunk = null;
		boolean equals = false;
		List<Line> diffs = new ArrayList<Line>();
		int changedLine = this.revLines.size();
		
		// Se buscan lineas donde se haya borrado un fragmento
		while (origLines.size() > 0 && !equals) {
			
			Line orig = origLines.get(0);
			
			for (Line rev : revLines) {
				if (orig.getText().equals(rev.getText())) {
					equals = true;
				}
			}
			// Si la linea es distinta, existe diferencia
			if (!equals) {
				diffs.add(orig);
				origLines.remove(0);
			}
		}
		// Si existen diferencias, se crea una instancia de DeleteDiffChunk
		if (diffs.size() > 0)
			chunk = new DeleteDiffChunk(diffs, changedLine);
		return chunk;
	}
	
	/**
	 * Elimina aquellas numeros de lineas a comparar que ya sean iguales.
	 * 
	 * @param origLine Lista de lineas del fichero original
	 * @param revLines Lista de lineas del fichero a revisar
	 */
	private void deleteEquals(List<Line> origLines, List<Line> revLines) {
		
		boolean equals = true;
		
		while (origLines.size() > 0 && revLines.size() > 0 && equals) {
			// Se eliminan aquellas lineas que son iguales
			if (origLines.get(0).getText().equals(revLines.get(0).getText())) {
				origLines.remove(0);
				revLines.remove(0);
			} else {
				equals = false;
			}
		}
	}
	
	/**
	 * Lectura del fichero para obtener el texto.
	 * 
	 * @param File Fichero de entrada.
	 * @return lines Lista de lineas con el texto del fichero leido
	 */
	private static List<String> getStringList(File file) {
		
		List<String> lines = new ArrayList<String>();
		Scanner scan = null;
		
		try {
			// Se decalara una instancia de la clase Scanner
			scan = new Scanner(file);
			// Mientras existan lineas se asignan a la lista
			while (scan.hasNextLine()) {
				lines.add(scan.nextLine());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (scan != null)
				// Se cierra la instancia de la clase Scanner
				scan.close();
		}
		return lines;
	}
}
