package uva.forense.forfat.diff.filediff;

import java.util.List;

/**
 * Representa un fragmento borrado en el archivo original para que se convierta en el de revision.
 * 
 * @author Alonso Mayo, Alejandro
 * @author Iglesias Garcia, Jesus
 * @version 1.0
 */
public class DeleteDiffChunk extends DiffChunk {

	/**
	 * Constructor de la clase DeleteDiffChunk.
	 * 
	 * @param difference Lista con lineas que presentan diferencias
	 * @param changedLine Linea del fichero revision donde se hayan borrado lineas
	 */
	public DeleteDiffChunk(List<Line> difference, int changedLine) {
		super(difference, changedLine);
	}

	/**
	 * Asocia a todos los objetos el texto representativo.
	 * 
	 * @return result Texto formateado a imprimir por pantalla
	 */
	@Override
	public String toString() {
		
		// Se obtienen la diferencias
		List<Line> difference = getDifference();
		
		// Se formatea la salida
		String result = "" + difference.get(0).getNumber();
		if (difference.size()>1)
			result += "," + difference.get(difference.size()-1).getNumber();
		result += "d" + getChangedLine() + "\n";
		
		// Se indica el texto diferente de cada diferencia encontrada
		for (Line line : difference) {
			result += "< " + line.getText() + "\n";
		}
		return result + "...";
	}

}
