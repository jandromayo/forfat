package uva.forense.forfat.diff.filediff;

import java.util.List;

/**
 * Representa una porcion de las diferencias entre un fichero original y su revision. El cambio,
 * representa un paso de lo que se necesitaria realizar para que el fichero
 * original se convierta en la revision.
 * 
 * @author Alonso Mayo, Alejandro
 * @author Iglesias Garcia, Jesus
 * @version 1.0
 */
public abstract class DiffChunk {

	// Declaracion de varibles
	private int changedLine;
	private List<Line> difference;

	/**
	 * Constructor de la clase DiffChunk.
	 * 
	 * @param difference Lista con lineas que presentan diferencias
	 * @param changedLine Linea modificada, el archivo depende del contexto
	 */
	public DiffChunk(List<Line> difference, int changedLine) {
		this.changedLine = changedLine;
		this.difference = difference;
	}
	
	/**
	 * Devuelve las diferencias entre el fichero original y el revisado.
	 * 
	 * @return Lista de diferencias
	 */
	public List<Line> getDifference() {
		return difference;
	}

	/**
	 * Devuelve la linea a partir de donde se realizan cambios para ajustarse a
	 * la revision.
	 * 
	 * @return Linea del fichero origen
	 */
	public int getChangedLine() {
		return changedLine;
	}

	/**
	 * Metodo que permite mostrar la informacion completa de un objeto.
	 * 
	 * @return Texto con los datos.
	 */
	@Override
	public abstract String toString();
}








