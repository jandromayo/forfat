package uva.forense.forfat.diff.filediff;

import java.util.List;

/**
 * Representa un fragmento insertado en el archivo original para que se convierta en el de revision.
 * 
 * @author Alonso Mayo, Alejandro
 * @author Iglesias Garcia, Jesus
 * @version 1.0
 */
public class AppendDiffChunk extends DiffChunk {

	/**
	 * Constructor de la clase AppendDiffChunk.
	 * 
	 * @param difference Lista con lineas que presentan diferencias
	 * @param changedLine Linea del fichero original donde se inserta el fragmento
	 */
	public AppendDiffChunk(List<Line> difference, int changedLine) {
		super(difference, changedLine);
	}

	/**
	 * Asocia a todos los objetos el texto representativo.
	 * 
	 * @return result Texto formateado a imprimir por pantalla
	 */
	@Override
	public String toString() {
		
		// Se obtienen las diferencias
		List<Line> difference = getDifference();
		
		// Se formatea la salida
		String result = getChangedLine() + "a" + difference.get(0).getNumber();		
		if (difference.size()>1)
			result += "," + difference.get(difference.size()-1).getNumber();		
		result += "\n";
		
		// Se indica el texto diferente de cada diferencia encontrado
		for (Line line : difference) {
			result += "> " + line.getText() + "\n";
		}
		return result + "...";
	}
}
