package uva.forense.forfat.diff.filediff;

/**
 * Representacion de una linea modificada ya sea insertada o borrada.
 * 
 * @author Alonso Mayo, Alejandro
 * @author Iglesias Garcia, Jesus
 * @version 1.0
 */
public class Line {
	
	// Declaracion de variables
	private int number;
	private String text;
	
	/**
	 * Contructor de la clase Line.
	 * 
	 * @param number Representa el numero de la linea
	 * @param text Representa el texto de la linea
	 */
	public Line(int number, String text) {
		super();
		this.number = number;
		this.text = text;
	}

	/**
	 * Devuelve el numero de una linea.
	 * 
	 * @return number Numero de la linea modificada
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * Devuelve el texto de una linea.
	 * 
	 * @return text Texto que contiene la linea
	 */
	public String getText() {
		return text;
	}
}
