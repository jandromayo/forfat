
package uva.forense.forfat.findext;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Clase que encuentra los ficheros de una determinada extension.
 * 
 * @author Alonso Mayo, Alejandro
 * @author Iglesias Garcia, Jesus
 * @version 1.0
 */
public class FindExt {

	// Declaracion de variables
	private String ext;
	private List<File> files = new ArrayList<File>();
	private File initFile;

	/**
	 * Constructor de la clase FindExt.
	 * 
	 * @param file Fichero donde se realiza la busqueda
	 * @param ext Extension a buscar
	 */
	public FindExt(File file, String ext) {
		this.ext = ext;
		this.initFile = file;
		find(file);
	}

	/**
	 * Copia los ficheros de los indices indicados en la ruta indicada.
	 * 
	 * @param references Lista de indices de los ficheros a copiar
	 * @param path Ruta donde se copian los ficheros
	 * 
	 * @throws Exception En caso de que haya errores en la copia
	 */
	public void copyFiles(List<Integer> references, String path) throws Exception {
		for (Integer i : references) {
			copy(files.get(i), path);
		}
	}

	/**
	 * Copia los ficheros de los indices indicados en la ruta indicada.
	 * 
	 * @param references Lista de indices de los ficheros a copiar
	 */
	private void find(File file) {
		
		// Se comprueba si el fichero es un directorio
		if (file.isDirectory()) {
			for (File f1 : file.listFiles()) {
				find(f1);
			}
		} else {
			// Se comprueba si el fichero contiene la extension. En caso afirmativo, se 
			// introduce en el ArrayList
			if (file.getName().endsWith("." + ext.replace(".", ""))) {
				files.add(file);
			}
		}
	}

	/**
	 * Implementacion de la copia de ficheros de los indices indicados en la ruta indicada.
	 * 
	 * @param file Fichero a copiar
	 * @param path Ruta donde se copian los ficheros
	 * 
	 * @throws Exception En caso de que haya errores en la copia
	 */
	private void copy(File file, String path) throws Exception {
		
		Scanner entrada = new Scanner (System.in); 
		File dir = new File(path);
		String nombre;
		int c, extension;
		
		// Se comprueba que la ruta destino exista y no sea un directorio
		if (dir.exists() && !dir.isDirectory()) {
			System.err.println("La ubicacion destino no es un directorio. Copia fallida.");
			System.exit(1);
		}

		File dir2 = new File(dir,
				file.getAbsolutePath()
						.replace("\\", "/")
						.replaceFirst(
								initFile.getAbsolutePath().replace("\\", "/")
										+ "/", "")
						.replaceAll(file.getName(), ""));
		
		// Si el directorio no existe, se crea
		if (!dir2.exists()) {
			dir2.mkdirs();
		}

		File copy = new File(dir2, file.getName());
		
		// Se comprueba si existe un fichero con el mismo nombre
		if (copy.exists()){
					
			do {
				System.out.print("Existe un fichero con el mismo nombre en la ruta indicada. Si desea mantener el fichero, indique su nombre y extension "
							+ "(Formato: <nombre.extension>) (en blanco para sobrescribir):");
				nombre = entrada.nextLine();
						
				// Se comprueba si el string tiene extension
				extension = nombre.lastIndexOf(".");
				
			} while( (extension == -1) &&  (nombre != null) && !(nombre.isEmpty()));
				
			// Se comprueba si no se introdujo el nuevo nombre
			if ( (nombre == null) || (nombre.isEmpty()) ) {
					
				FileInputStream in = new FileInputStream(file);
				FileOutputStream out = new FileOutputStream(copy);
						
				// Se copia del contenido del fichero origen al fichero destino
				while ((c = in.read()) != -1)
				out.write(c);

				// Se cierran ambos ficheros
				in.close();
				out.close();
					
			} else{ // Nuevo nombre introducido
						
				File copy2 = new File(dir2, nombre);
						
				FileInputStream in = new FileInputStream(file);
				FileOutputStream out = new FileOutputStream(copy2);
						
				// Se copia del contenido del fichero origen al fichero destino
				while ((c = in.read()) != -1)
				out.write(c);
						
				// Se cierran ambos ficheros
				in.close();
				out.close();
			}
		} else { // No existe un fichero con el mismo nombre	
						
			FileInputStream in = new FileInputStream(file);
			FileOutputStream out = new FileOutputStream(copy);
				
			// Se copia del contenido del fichero origen al fichero destino
			while ((c = in.read()) != -1)
				out.write(c);
					
			// Se cierran ambos ficheros
			in.close();
			out.close();
			}
				
		// Se cierra el scanner
		entrada.close();
	}

	/**
	 * Devuelve la lista de ficheros
	 * 
	 * @return Copia de la lista de ficheros.
	 */
	public List<File> getFiles() {
		return files.subList(0, files.size());
	}

}
